import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-error-code',
  templateUrl: './error-code.component.html',
  styleUrls: ['./error-code.component.scss']
})
export class ErrorCodeComponent implements OnInit {
  errorCodeForm: FormGroup;
  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.errorCodeForm = this.fb.group({
      errorCode: ['', Validators.required],
      errorCategory: [null, Validators.required],
      errorDescription: [''],
      clientErrorCode: [''],
      clientErrorDescription: ['']
    })
  }

}
