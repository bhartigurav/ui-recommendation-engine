import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValidationRuleSetupComponent } from './validation-rule-setup.component';

describe('ValidationRuleSetupComponent', () => {
  let component: ValidationRuleSetupComponent;
  let fixture: ComponentFixture<ValidationRuleSetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValidationRuleSetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValidationRuleSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
