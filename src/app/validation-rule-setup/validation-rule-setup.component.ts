import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { codeValidator } from '../validations/code-validators';
import { genericValidator } from '../validations/generic-validators';

@Component({
  selector: 'app-validation-rule-setup',
  templateUrl: './validation-rule-setup.component.html',
  styleUrls: ['./validation-rule-setup.component.scss']
})
export class ValidationRuleSetupComponent implements OnInit {
  validationRuleForm: FormGroup;
  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.validationRuleForm = this.fb.group({
      ruleCode: ['', [Validators.required, codeValidator.pattern(/^([\w._\s\/-]*)$/)]],
      ruleName: ['', [Validators.required, genericValidator.pattern(/^([\w.\s0-9]*)$/)]],
      ruleDescription: ['', Validators.required],
      ruleBucket: [null, Validators.required],
      fieldCode: [null, Validators.required],
      operator: [null, Validators.required],
      negate: [''],
      comparator: [null, Validators.required],
      valueType: [null, Validators.required],
      value: ['', Validators.required],
      linkedToField: ['', Validators.required]
    });

    (<AbstractControl>this.validationRuleForm.controls['linkedToField']).valueChanges.subscribe(value => {
      if( value === 'no') {
        (<AbstractControl>this.validationRuleForm.controls['fieldCode']).disable();
      } else {
        (<AbstractControl>this.validationRuleForm.controls['fieldCode']).enable();
      }
    })
  }

  get vRuleForm() { return this.validationRuleForm.controls; }

}
