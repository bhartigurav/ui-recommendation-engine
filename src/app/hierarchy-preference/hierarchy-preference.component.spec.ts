import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HierarchyPreferenceComponent } from './hierarchy-preference.component';

describe('HierarchyPreferenceComponent', () => {
  let component: HierarchyPreferenceComponent;
  let fixture: ComponentFixture<HierarchyPreferenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HierarchyPreferenceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HierarchyPreferenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
