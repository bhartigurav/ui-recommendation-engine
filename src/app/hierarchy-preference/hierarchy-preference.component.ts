import { Component, OnInit, OnChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EntityPreference } from "../entity-preference";

@Component({
  selector: 'app-hierarchy-preference',
  templateUrl: './hierarchy-preference.component.html',
  styleUrls: ['./hierarchy-preference.component.scss']
})
export class HierarchyPreferenceComponent implements OnInit, OnChanges {

  hierarchyPreferenceForm: FormGroup;
  entityPreference: EntityPreference;;
  isEntityPreferenceAvailable: boolean;
  entityPrefArr: EntityPreference[];

  constructor(private fb: FormBuilder) {

    this.isEntityPreferenceAvailable = false;
    this.entityPreference = {} as EntityPreference;
    this.entityPrefArr = [] as EntityPreference[];

   }

  ngOnInit() {

    this.hierarchyPreferenceForm = this.fb.group({
      hierarchyEntityType: [null, Validators.required],
      hierarchyEntityCode: [null, Validators.required],
      disclaimerBucket: [null],
      disclaimerRule: [null],
      advisingBucket: [null],
      advisingRule: [null],
      marketingBucket: [null],
      marketingRule: [null],
      decisionCode: [null, Validators.required],
      scoreMultiplier: [null, Validators.required]
    });

  }

  ngOnChanges() {

  }

  get hpForm() { return this.hierarchyPreferenceForm.controls; }

  public addEntityPreference() {

    this.entityPreference.decisionCode = this.hpForm.decisionCode.value;
    this.entityPreference.scoreMultiplier = this.hpForm.scoreMultiplier.value;

    this.entityPrefArr.push(this.entityPreference);
    this.entityPreference = {} as EntityPreference;

    if(!this.isEntityPreferenceAvailable) {

      this.isEntityPreferenceAvailable = true;

    }
  }

}
