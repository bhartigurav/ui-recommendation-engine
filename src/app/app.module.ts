import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { NgSelectModule } from '@ng-select/ng-select';
import { ClarityModule } from "@clr/angular";
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HeaderComponent } from './header/header.component';
import { SampleComponent } from './sample/sample.component';
import { IndexComponent } from './index/index.component';
import { HierarchyPreferenceComponent } from './hierarchy-preference/hierarchy-preference.component';
import { DecisionParamComponent } from './decision-param/decision-param.component';
import { EnrichmentRuleSetupComponent } from './enrichment-rule-setup/enrichment-rule-setup.component';
import { OfferingComponent } from './offering/offering.component';
import { DynamicApiComponent } from './dynamic-api/dynamic-api.component';
import { ExternalSystemComponent } from './external-system/external-system.component';
import { FieldMasterComponent } from './field-master/field-master.component';
import { ListConfigComponent } from './list-config/list-config.component';
import { QueryConfigComponent } from './query-config/query-config.component';
import { ValidationRuleSetupComponent } from './validation-rule-setup/validation-rule-setup.component';
import { ErrorCodeComponent } from './error-code/error-code.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SampleComponent,
    IndexComponent,
    HierarchyPreferenceComponent,
    DecisionParamComponent,
    EnrichmentRuleSetupComponent,
    OfferingComponent,
    DynamicApiComponent,
    ExternalSystemComponent,
    FieldMasterComponent,
    ListConfigComponent,
    QueryConfigComponent,
    ValidationRuleSetupComponent,
    ErrorCodeComponent
  ],
  imports: [
    BrowserModule,
    NgSelectModule,
    ClarityModule,
    ReactiveFormsModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
