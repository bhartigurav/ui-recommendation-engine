import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicApiComponent } from './dynamic-api.component';

describe('DynamicApiComponent', () => {
  let component: DynamicApiComponent;
  let fixture: ComponentFixture<DynamicApiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DynamicApiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicApiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
