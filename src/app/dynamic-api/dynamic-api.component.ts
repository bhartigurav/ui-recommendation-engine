import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, AbstractControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-dynamic-api',
  templateUrl: './dynamic-api.component.html',
  styleUrls: ['./dynamic-api.component.scss']
})
export class DynamicApiComponent implements OnInit {
  dynamicApiForm: FormGroup;

  constructor(private fb: FormBuilder) {
  }

  ngOnInit() {
    this.dynamicApiForm = this.fb.group({
      systemCode: [null, Validators.required],
      apiName: ['', Validators.required],
      sslEnabled: [''],
      authSecretKey: [''],
      httpUri: [''],
      messageTemplatized: [''],
      configReqResp: [''],
      parameterType: [''],
      paramKeyOrJsonPath: [''],
      httpMethod: [null],
      mappedCode: [''],
      singleRowOutput: [''],
      bodyTemplate: ['']
    });

    // (<AbstractControl>this.dynamicApiForm.controls['parameterType']).valueChanges.subscribe(value => {
    //   if(value !== 'body') {
    //     this.dynamicApiForm.get('jsonPath').disable();
    //     this.dynamicApiForm.get('mappedFieldCode').disable();
    //     this.dynamicApiForm.get('jsonPath').setValue('');
    //     this.dynamicApiForm.get('mappedFieldCode').setValue('');
    //   } else {
    //     this.dynamicApiForm.get('jsonPath').enable();
    //     this.dynamicApiForm.get('mappedFieldCode').enable();
    //   }

    // })

  }

}
