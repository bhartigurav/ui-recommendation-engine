import { Component, OnInit, ViewChild, ElementRef, ViewContainerRef } from '@angular/core';
import { ClrWizard } from '@clr/angular';
import { FormGroup, FormBuilder, AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-offering',
  templateUrl: './offering.component.html',
  styleUrls: ['./offering.component.scss']
})
export class OfferingComponent implements OnInit {
  offeringCodeForm: FormGroup;
  // @ViewChild('wizard', { static: false })
  // wizard: ClrWizard;

  public open: boolean = true;
  constructor(private fb: FormBuilder) { }

  ngOnInit() {

    this.offeringCodeForm = this.fb.group({
      offeringDefinition: this.fb.group({
        offeringCode: [''],
        entityCode: [null],
        productCode: [null],
        subProductCode: [''],
        offeringName: [''],
        offeringDescription: [''],
        disclaimerRule: [null],
        disclaimerBucket: [null],
        advisingBucket: [null],
        advisingRule: [null],
        marketingBucket: [null],
        marketingRule: [null]
      }),
      offeringFields: this.fb.group({
        fieldBucket: [null],
        offeringField: [null],
        mandatoryOptional: [null],
        inputDerivedFulfillment: [null]
      }),
      derivedFields: this.fb.group({
        offeringField: [null],
        enrichmentRule: [null],
        validationRule: [null],
        errorRaised: [''],
        errorCode: [null]
      }),
      eliminationRules: this.fb.group({
        validationRule: [null],
        errorRaised: [''],
        errorCode: [null]
      })
    });

    (<AbstractControl>this.derivedFields.controls['errorRaised']).valueChanges.subscribe(value => {
      if(value === true) {
        (<AbstractControl>this.derivedFields.controls['errorCode']).disable();
      } else {
        (<AbstractControl>this.derivedFields.controls['errorCode']).enable();
      }
    });

    (<AbstractControl>this.eliminationRules.controls['errorRaised']).valueChanges.subscribe(value => {
      if(value === true) {
        (<AbstractControl>this.eliminationRules.controls['errorCode']).disable();
      } else {
        (<AbstractControl>this.eliminationRules.controls['errorCode']).enable();
      }
    });

  }

  get derivedFields() {
    return this.offeringCodeForm.get('derivedFields') as FormGroup;
  }

  get eliminationRules() {
    return this.offeringCodeForm.get('eliminationRules') as FormGroup;
  }

}
