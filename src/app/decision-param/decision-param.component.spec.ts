import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DecisionParamComponent } from './decision-param.component';

describe('DecisionParamComponent', () => {
  let component: DecisionParamComponent;
  let fixture: ComponentFixture<DecisionParamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DecisionParamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DecisionParamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
