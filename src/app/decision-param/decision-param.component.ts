import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-decision-param',
  templateUrl: './decision-param.component.html',
  styleUrls: ['./decision-param.component.scss']
})
export class DecisionParamComponent implements OnInit {
  decisionParamForm: FormGroup;
  ruleBucketData: string[];

  constructor(private fb: FormBuilder) { }

  ngOnInit() {

    this.decisionParamForm = this.fb.group({
      decisionCode: ['', Validators.required],
      decisionLabel: ['', Validators.required],
      superlativeLabel: ['', Validators.required],
      inferiorLabel: ['', Validators.required],
      scoringRule: [null, Validators.required],
      ruleBucket: [null, Validators.required],
      commonalityFactor: ['', Validators.required]
    });

    this.ruleBucketData = ["Rule bucket 1", "Rule bucket 2", "Rule bucket 3"]

  }

}
