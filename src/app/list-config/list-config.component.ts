import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-list-config',
  templateUrl: './list-config.component.html',
  styleUrls: ['./list-config.component.scss']
})
export class ListConfigComponent implements OnInit {

  listConfigForm: FormGroup;
  listSourceData: Array<object>;
  listSourceType: string;
  listSeq: number;

  constructor(private fb: FormBuilder) {

    this.listSeq = 1;
    this.listSourceType = '';

    this.listSourceData = [
      {
        title: "This screen",
        value: "This screen"
      },
      {
        title: "Query",
        value: "Query"
      },
      {
        title: "Dynamic API",
        value: "Dynamic API"
      }
    ];


  }

  ngOnInit() {

    this.listConfigForm = this.fb.group({
      listCode: ['', Validators.required],
      listName: [''],
      listSource: [null],
      queryCode: [''],
      dynamicApi: ['']
    });

    (<AbstractControl>this.listConfigForm.controls['listSource']).valueChanges.subscribe(data => {

      if(data) {

        this.listSourceType = data.value;

      } else {

        this.listSourceType = '';

      }

    });

  }



}
