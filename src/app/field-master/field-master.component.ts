import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-field-master',
  templateUrl: './field-master.component.html',
  styleUrls: ['./field-master.component.scss']
})
export class FieldMasterComponent implements OnInit {
  fieldMasterForm: FormGroup;
  constructor(private fb: FormBuilder) {
    this.fieldMasterForm = this.fb.group({
      fieldCode: ['', Validators.required],
      fieldName: [''],
      dataType: [null],
      dataLength: [''],
      fieldBucket: [null],
      validationRule: [null]
    });
   }

  ngOnInit() {
  }

}
