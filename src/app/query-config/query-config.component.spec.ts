import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QueryConfigComponent } from './query-config.component';

describe('QueryConfigComponent', () => {
  let component: QueryConfigComponent;
  let fixture: ComponentFixture<QueryConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QueryConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QueryConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
