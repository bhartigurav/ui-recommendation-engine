import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-query-config',
  templateUrl: './query-config.component.html',
  styleUrls: ['./query-config.component.scss']
})
export class QueryConfigComponent implements OnInit {

  queryConfigForm: FormGroup;
  public listSeq: number;
  constructor(private fb: FormBuilder) {
    this.listSeq = 1;
  }

  ngOnInit() {

    this.queryConfigForm = this.fb.group({
      queryCode: ['', Validators.required],
      queryName: ['', Validators.required],
      queryDescription: [''],
      queryType: [''],
      dataSource: [null],
      queryString: [''],
      returnValueAlias: [''],
      queryCode1: [''],
      sequence: [null],
      variable: [null],
      fieldCodeMapping: [null],
      singleRowOutput: ['']
    });

  }

}
