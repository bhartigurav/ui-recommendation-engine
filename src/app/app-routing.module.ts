import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SampleComponent } from './sample/sample.component';
import { IndexComponent } from './index/index.component';
import { HierarchyPreferenceComponent } from './hierarchy-preference/hierarchy-preference.component';
import { DecisionParamComponent } from './decision-param/decision-param.component';
import { EnrichmentRuleSetupComponent } from './enrichment-rule-setup/enrichment-rule-setup.component';
import { OfferingComponent } from './offering/offering.component';
import { DynamicApiComponent } from './dynamic-api/dynamic-api.component';
import { ExternalSystemComponent } from './external-system/external-system.component';
import { FieldMasterComponent } from './field-master/field-master.component';
import { ListConfigComponent } from './list-config/list-config.component';
import { QueryConfigComponent } from './query-config/query-config.component';
import { ValidationRuleSetupComponent } from './validation-rule-setup/validation-rule-setup.component';
import { ErrorCodeComponent } from './error-code/error-code.component';


const routes: Routes = [
  { path: '', component: IndexComponent },
  { path: 'sample', component: SampleComponent },
  { path: 'hierarchy-preference', component: HierarchyPreferenceComponent },
  { path: 'decision-param', component: DecisionParamComponent },
  { path: 'enrichment-rule-setup', component: EnrichmentRuleSetupComponent },
  { path: 'offering', component: OfferingComponent },
  { path: 'dynamic-api', component: DynamicApiComponent },
  { path: 'interfacing-system', component: ExternalSystemComponent },
  { path: 'field-master', component: FieldMasterComponent },
  { path: 'list-config', component: ListConfigComponent },
  { path: 'query-config', component: QueryConfigComponent },
  { path: 'validation-rule-setup', component: ValidationRuleSetupComponent },
  { path: 'error-code', component: ErrorCodeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
