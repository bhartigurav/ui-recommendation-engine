import { ValidatorFn, AbstractControl } from '@angular/forms';
export class codeValidator {
  public static pattern(code: RegExp): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      const allowed = code.test(control.value);
      return !allowed ? { validCode: true } : null;
    };
  }
}
