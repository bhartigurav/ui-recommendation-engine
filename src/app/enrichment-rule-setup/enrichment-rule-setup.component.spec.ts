import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnrichmentRuleSetupComponent } from './enrichment-rule-setup.component';

describe('EnrichmentRuleSetupComponent', () => {
  let component: EnrichmentRuleSetupComponent;
  let fixture: ComponentFixture<EnrichmentRuleSetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnrichmentRuleSetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnrichmentRuleSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
