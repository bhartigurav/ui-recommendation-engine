import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-enrichment-rule-setup',
  templateUrl: './enrichment-rule-setup.component.html',
  styleUrls: ['./enrichment-rule-setup.component.scss']
})
export class EnrichmentRuleSetupComponent implements OnInit {
  enrichmentRuleForm: FormGroup;
  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.enrichmentRuleForm = this.fb.group({
      ruleCode: ['', Validators.required],
      ruleName: ['', Validators.required],
      ruleDescription: ['', Validators.required],
      ruleBucket: [null, Validators.required],
      enrichmentTypeElse: [null, Validators.required],
      enrichmentValueElse: ['', Validators.required],
      sequence: [null, Validators.required],
      validationRule: [null, Validators.required],
      validationRuleBucket: [null, Validators.required],
      enrichmentTypeIf: [null, Validators.required],
      enrichmentValueIf: [null, Validators.required]
    })
  }

}
