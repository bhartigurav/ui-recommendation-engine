export interface EntityPreference {
    decisionCode: string;
    scoreMultiplier: string;
}
