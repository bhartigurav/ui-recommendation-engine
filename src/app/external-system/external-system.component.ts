import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, AbstractControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-external-system',
  templateUrl: './external-system.component.html',
  styleUrls: ['./external-system.component.scss']
})
export class ExternalSystemComponent implements OnInit {
  externalSystemForm: FormGroup;
  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.connectionType = "";
    this.externalSystemForm = this.fb.group({
      systemCode: ['', Validators.required],
      systemName: ['', Validators.required],
      systemLevel: ['', Validators.required],
      connectionType: ['', Validators.required],
      seviceName: [''],
      domainName: [''],
      portNumber: ['']
    });

    (<AbstractControl>this.externalSystemForm.controls['connectionType']).valueChanges.subscribe(data => {
      this.connectionType = data;
      if(data === 'serviceDiscovery') {

        (<AbstractControl>this.externalSystemForm.controls['seviceName']).setValidators([Validators.required]);

      }

      if(data === 'dnsBased') {

        (<AbstractControl>this.externalSystemForm.controls['domainName']).setValidators([Validators.required]);
        (<AbstractControl>this.externalSystemForm.controls['portNumber']).setValidators([Validators.required]);

      }

    });
  }

  connectionType: string;


}
